package jpabook.jpashop;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;  //자동 import되지 않음

@ExtendWith(SpringExtension.class)
@SpringBootTest
class MemberRepositoryTest {
    @Autowired
    MemberRepository_ memberRepository;

    @Test
    @Transactional //    테스트 종료 후 데이터 처리 롤백
    @Rollback(value = false) // 테스트 종료 후 롤백처리 하지 않게 해야 쿼리가 보인다.
    public void testMember() throws Exception {
        //giver
        Member_ member = new Member_();
        member.setUsername("memberA");

        //when
        Long savedId = memberRepository.save(member);
        System.out.println("savedId : " + savedId);
        Member_ findMember = memberRepository.find(savedId);
        System.out.println(findMember.toString());


        //then
        assertThat(findMember.getId()).isEqualTo(member.getId());
        assertThat(findMember.getUsername()).isEqualTo(member.getUsername());
        assertThat(findMember).isEqualTo(member);
        System.out.println("findMember == member : " + (findMember == member));
        System.out.printf("here Call............");

    }

}