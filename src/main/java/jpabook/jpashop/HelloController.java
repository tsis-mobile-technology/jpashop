package jpabook.jpashop;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HelloController {
    @GetMapping("hello") // hello라는 URL로 오면 여기를 타게 한다.
    public String hello(Model model) {
        model.addAttribute("data", "hello!!!");
        return "hello"; //  return "hello" 라는게 "templates" 디렉토리에서 hello.html을 찾는다.
    }
}
