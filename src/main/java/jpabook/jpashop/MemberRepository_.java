package jpabook.jpashop;

import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class MemberRepository_ {

    @PersistenceContext
    private EntityManager em;

    public Long save(Member_ member) {
        em.persist(member);
        return member.getId();
    }

    public Member_ find(Long id) {
        return em.find(Member_.class, id);
    }
}
