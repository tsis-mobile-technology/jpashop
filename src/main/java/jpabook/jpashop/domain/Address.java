package jpabook.jpashop.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Embeddable;

@Embeddable
@Getter
// Doamin에 셋팅한 Getter, Setter랑  설정은 하는데 실무 에서는 Setter의 경우 Public으로 오픈하지 않는다. 추후
// 관리가 힘들수 있다.
public class Address {
    private String city;
    private String street;
    private String zipcode;

    protected Address() {
    }

    public Address(String city, String street, String zipcode) {
        this.city = city;
        this.street = street;
        this.zipcode = zipcode;

    }
}